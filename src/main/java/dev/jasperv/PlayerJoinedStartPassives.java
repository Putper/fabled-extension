package dev.jasperv;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitScheduler;
import studio.magemonkey.fabled.Fabled;
import studio.magemonkey.fabled.api.player.PlayerData;

/**
 * Manually start all passive Fabled skills of a player after they join the server.
 * To fix an issue we're having with Passive mechanics not being triggered after a player rejoins the server.
 */
public class PlayerJoinedStartPassives implements Listener
{
    /**
     * delay
     * Is in ticks, 20ticks = 1 second.
     */
    static final long TICKS_DELAY_AFTER_JOIN = 20L; // 1 second

    final private Main _plugin;

    public PlayerJoinedStartPassives(Main plugin)
    {
        this._plugin = plugin;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event)
    {
        Player player = event.getPlayer();
        startPassives(player);
    }

    /**
     * Start all Fabled Passive skills of a given player
     * @param player player to start passive skills for
     */
    private void startPassives(Player player)
    {
        BukkitScheduler scheduler = Bukkit.getScheduler();

        scheduler.runTaskLater(this._plugin, () ->
        {
            PlayerData data = Fabled.getData(player);
            data.startPassives(player);
        }, TICKS_DELAY_AFTER_JOIN);
    }
}
