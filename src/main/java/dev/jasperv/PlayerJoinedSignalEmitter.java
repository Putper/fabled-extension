package dev.jasperv;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;
import studio.magemonkey.fabled.Fabled;
import studio.magemonkey.fabled.api.event.SignalEmitEvent;
import studio.magemonkey.fabled.api.player.PlayerData;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * When a player joins the game,
 * a Fabled Signal is emitted with "player-joined".
 * Signals can be caught with a Signal Trigger in Fabled.
 */
public class PlayerJoinedSignalEmitter implements Listener
{
    /**
     * Delay in ticks before signal is sent
     */
    static final long SIGNAL_TICKS_DELAY = 300L; // 20ticks = 1 second

    private Plugin _plugin;

    public PlayerJoinedSignalEmitter(Plugin plugin)
    {
        _plugin = plugin;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event)
    {
        Player player = event.getPlayer();
        String signal = "player-joined";

        BukkitScheduler scheduler = Bukkit.getScheduler();
        scheduler.runTaskLater(this._plugin, () ->
        {
            emitSignal(signal, player);
        }, SIGNAL_TICKS_DELAY);
    }

    /**
     * Calls a custom Fabled signal. Same as the "Signal Emit" mechanic in Fabled.
     * Can be caught using a Signal trigger in Fabled.
     * @param signal the signal that will be emitted
     * @param player player that emitted the signal, marked as both emitter and recipient/target
     */
    private void emitSignal(String signal, Player player)
    {
        List<Object> arguments = new ArrayList<>();
        arguments.add(player);

        Bukkit.getPluginManager().callEvent(
                new SignalEmitEvent(null, player, player, signal, arguments, true)
        );
    }
}
