package dev.jasperv;

import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin
{
    @Override
    public void onEnable()
    {
        getServer().getPluginManager().registerEvents(new PlayerJoinedSignalEmitter(this), this);
        getServer().getPluginManager().registerEvents(new PlayerJoinedStartPassives(this), this);
        getLogger().info("Enabled jasper's Fabled Extension");

    }

    @Override
    public void onDisable()
    {
        getLogger().info("Disabled jasper's Fabled Extension");
    }
}
